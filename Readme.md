# Cal's Skeleton WordPress for Lando project

Cal Evans<br >cal@calevans.com

# Instructions

1. Install Lando and make sure it's working
1. Make a directory for your project
1. Run `composer create-project eicc/wp-for-lando YOUR/NEW/DIR`
1. Edit the `.lando.yml` file. At the very least, change the name of the projet to whatever
1. `lando start`
1. **Optional:** Copy `scripts.env.sample` to `scripts/.env`
1. **Optional:** Edit `scripts/.env`. IF you lan on cloneing a WP site down then you need to fill this out
1. **Optional:**  Run `lando clonewp` The first time, you run it without any arguments and it will clone the entire site. After that you can use `-d` to ONLY replace the database or `-f` to ONLY replace the files. Both of these are destructive.

## `clone.sh`

At the bottom of `clone.sh` you will see a place to add extra commands. These will be run insode of the container, not on the host OS. This is where you can disable/delete plugins that are in production that you don't want in development.

**NOTE: There is NO WAY to push the DB or files up to production form development using this package. You'll need to figure that one out on your own.

## `scripts/.env`

This is only necessary if you plan on cloning an existing site down to work on it. If you are planning on using hte clone feature then you need to fill out each field.

- `ORIGIN_HOST`<br />
IP address or machine name of the host machine. This MAY be the same as the domain anme, and then again, it may not.
- `ORIGIN_USERNAME`<br />
ssh user name
- `ORIGIN_PRIVATE_KEY`<br />
The location and name of the key INSIDE the container. This usually looks something like /user/.ssh/id_rsa
- `ORIGIN_DOMAIN`<br />
The domain name of the WP site  e.g www.example.com or example.com
- `ORIGIN_PATH`<br />
Webroot on the production server /var/www/example.com/public_html
- `ORIGIN_PORT`<br />
SSH Port number

## WEBROOT

The `.land.yml` is set to use `public_html` as the web root. If you want to change that do so BEFORE you run `lando start`. It is hardcoded in the following scripts:

- `.lando.yml`
- `scripts/build.sh`
- `scripts/clone.sh`

# Support

None. Nada. Zilch.

If you have a problem, figure it out and then let us know the solution. This is one of those "it works for me" projects. :)
