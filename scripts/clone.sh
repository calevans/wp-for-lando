#! /bin/bash
#
# @todo figure out what web root is on the origin server because that's what it's going to be on the dest and we've got things that need to change if it's not public_html.
#
source scripts/helper_functions.sh

echo
green_text "WP Clone (Lando)"
yellow_text "================"
green_text "(c) 2022  Cal Evans <cal@calevans.com>"
green_text "A script to clone a production WordPress website down to a Lando based"
green_text "development environment"
echo

yellow_text "Setting up variables"
#
# All the changable environment variables are in the .env file.
#
source scripts/.env

COPY_DATABASE=false
COPY_FILES=false

#
# Test for arguments
#   -d copy the database
#   -f copy the files
#
# No argument === copy both
#
if  [[ $# -lt 1 ]]
then
  COPY_DATABASE=true
  COPY_FILES=true
fi;

for LCVA in $@
do
	if [[ "$COPY_DATABASE" == false ]] && [[ "$LCVA" =~ ^(-d)$ ]]; then
    COPY_DATABASE=true
  fi;
	if [[ "$COPY_FILES" == false ]] && [[ "$LCVA" =~ ^(-f)$ ]]; then
    COPY_FILES=true
  fi;
done

if [[ "$COPY_DATABASE" == true ]]
then
    green_text "Set to copy database"
fi;

if [[ "$COPY_FILES" == true ]]
then
    green_text "Set to copy files"
fi;


DEST_DOMAIN=$(php -r 'echo parse_url(json_decode($_SERVER["LANDO_INFO"])->appserver->urls[0])["host"];')
DEST_HOST=$DEST_DOMAIN

ORIGIN_SSH_COMMAND="-l $ORIGIN_USERNAME -p $ORIGIN_PORT -i $ORIGIN_PRIVATE_KEY $ORIGIN_HOST"
WEBROOT=$(basename $ORIGIN_PATH);
DEST_WEBROOT="/app/$WEBROOT"

yellow_text "Testing for a sane environment"

if [ -z "$(ssh $ORIGIN_SSH_COMMAND "pwd")" ]
then
  red_text "ERROR!"
  red_text "There is no ssh access to the origin host"
  exit 1
fi

ORIGIN_WP_SSH="--ssh=$ORIGIN_USERNAME@$ORIGIN_HOST:$ORIGIN_PORT$ORIGIN_PATH"

if [ -z "$(wp cli version --quiet $ORIGIN_WP_SSH)" ]
then
  red_text "ERROR!"
  red_text "There is no wp-cli on the origin host"
  exit 3
fi


green_text "We are good to go."

yellow_text "Gathering Origin DB Information"
ORIGIN_DB_NAME=$(wp config get DB_NAME $ORIGIN_WP_SSH | perl -ne 'chomp and print')
ORIGIN_DB_USER=$(wp config get DB_USER $ORIGIN_WP_SSH | perl -ne 'chomp and print')
ORIGIN_DB_PASSWORD=$(wp config get DB_PASSWORD $ORIGIN_WP_SSH | perl -ne 'chomp and print')

ORIGIN_DB_HOST=$(wp config get DB_HOST $ORIGIN_WP_SSH)

yellow_text "Gathering Destination DB Information"
DEST_DB_NAME=$(wp config get DB_NAME | perl -ne 'chomp and print')
DEST_DB_USER=$(wp config get DB_USER | perl -ne 'chomp and print')
DEST_DB_PASSWORD=$(wp config get DB_PASSWORD | perl -ne 'chomp and print')
DEST_DB_HOST=$(wp config get DB_HOST | perl -ne 'chomp and print')

# copy the files
if [[ $COPY_FILES == true ]]
then
  yellow_text "Transfering files"

  if [[ ! -d "$DEST_WEBROOT" ]]
  then
    mkdir $DEST_WEBROOT
  fi;

  cd $DEST_WEBROOT; ssh $ORIGIN_SSH_COMMAND "cd $ORIGIN_PATH;tar cz ./" | tar xz

  yellow_text "Updating wp-config.php settings"
  wp config set DB_NAME $DEST_DB_NAME --quiet $DEST_WP_SSH
  wp config set DB_USER $DEST_DB_USER --quiet $DEST_WP_SSH
  wp config set DB_PASSWORD $DEST_DB_PASSWORD --quiet $DEST_WP_SSH
  wp config set DB_HOST $DEST_DB_HOST --quiet $DEST_WP_SSH
fi;

# DB
if [[ $COPY_DATABASE == true ]]
then
  yellow_text "Resetting the destination database"
  wp db reset --yes --quiet $DEST_WP_SSH
  yellow_text "Transfering the database"
  ssh $ORIGIN_SSH_COMMAND \
    mysqldump -u $ORIGIN_DB_USER -p$ORIGIN_DB_PASSWORD --add-drop-table --add-locks --create-options --quick --skip-lock-tables --extended-insert --set-charset --disable-keys $ORIGIN_DB_NAME \
    | wp db import - $DEST_WP_SSH

  yellow_text "Search/Replace"
  wp search-replace $ORIGIN_DOMAIN $DEST_DOMAIN --quiet
fi;

#
# Add any final things that need to be done here.
#
#
# SiteGround puts this header in and Apache chokes on it. So comment it out.
#
if [[ -f "$DEST_WEBROOT/.htaccess" ]]
then
  perl -p -i -e 's/^Header unset Vary/# Header unset Vary/' $DEST_WEBROOT/.htaccess
fi;

#
# Turn off uneeded plugins
#
# wp plugin deactivate wp-2fa sg-cachepress sg-security hellowoofy-com

#
# Cleanup
#
wp cache flush

echo
green_text "Done"
echo

