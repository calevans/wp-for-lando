#!/usr/bin/env bash
export PATH="$PATH:/app/public/vendor/bin:/app/.composer/vendor/bin"

# Setup basic and TMF packages
apt-get update \
	&& apt-get install -y \
		less \
		vim \
		nano \
		jq \
    make \
    git \
  && apt-get remove -y build-essential

